
https://docs.nvidia.com/networking/display/mlnxofedv582030lts/rdma+over+converged+ethernet+(roce)

GID table entries are created whenever an IP address is configured on one of the Ethernet devices of the NIC's ports. Each entry in the GID table for RoCE ports has the following fields:


GID table is exposed to userspace via sysfs.

- GID value
/sys/class/infiniband/{device}/ports/{port}/gids/{index}

- GID type
/sys/class/infiniband/{device}/ports/{port}/gid_attrs/types/{index}

- Network device
/sys/class/infiniband/{device}/ports/{port}/gid_attrs/ndevs/{index}

```
[root@rdma-qe-40 ~]$ cat /sys/class/infiniband/mlx5_1/ports/1/gid_attrs/types/0
IB/RoCE v1
[root@rdma-qe-40 ~]$ cat /sys/class/infiniband/mlx5_1/ports/1/gid_attrs/ndevs/0
Mlx5_roce
```




