```
iWarp uses TCP, while RoCE uses UDP.

If it’s RoCE, then transport:			InfiniBand (0),
If it’s iWarp, then transport:			iWARP (1)

+ [22-05-31 06:02:34] /usr/bin/ibv_devinfo -v
hca_id:	qedr0
	transport:			InfiniBand (0)
	fw_ver:				8.59.1.0
	node_guid:			020e:1eff:fed4:227e
	sys_image_guid:			020e:1eff:fed4:227e
	vendor_id:			0x1077
	vendor_part_id:			5718
	hw_ver:				0x1
	phys_port_cnt:			1
	max_mr_size:			0x10000000000
	page_size_cap:			0xfffff000
	max_qp:				8568
	max_qp_wr:			32767
	device_cap_flags:		0x00309080
					CURR_QP_STATE_MOD
					RC_RNR_NAK_GEN
					XRC
					MEM_MGT_EXTENSIONS
					Unknown flags: 0x8000
	max_sge:			4
	max_sge_rd:			4
	max_cq:				17136
	max_cqe:			8388480
	max_mr:				131070
	max_pd:				65536
	max_qp_rd_atom:			32
	max_ee_rd_atom:			0
	max_res_rd_atom:		0
	max_qp_init_rd_atom:		32
	max_ee_init_rd_atom:		0
	atomic_cap:			ATOMIC_GLOB (2)
	max_ee:				0
	max_rdd:			0
	max_mw:				0
	max_raw_ipv6_qp:		0
	max_raw_ethy_qp:		0
	max_mcast_grp:			0
	max_mcast_qp_attach:		0
	max_total_mcast_qp_attach:	0
	max_ah:				8568
	max_fmr:			0
	max_srq:			8192
	max_srq_wr:			32767
	max_srq_sge:			0
	max_pkeys:			1
	local_ca_ack_delay:		15
	general_odp_caps:
	rc_odp_caps:
					NO SUPPORT
	uc_odp_caps:
					NO SUPPORT
	ud_odp_caps:
					NO SUPPORT
	xrc_odp_caps:
					NO SUPPORT
	completion_timestamp_mask not supported
	core clock not supported
	device_cap_flags_ex:		0x309080
	tso_caps:
		max_tso:			0
	rss_caps:
		max_rwq_indirection_tables:			0
		max_rwq_indirection_table_size:			0
		rx_hash_function:				0x0
		rx_hash_fields_mask:				0x0
	max_wq_type_rq:			0
	packet_pacing_caps:
		qp_rate_limit_min:	0kbps
		qp_rate_limit_max:	0kbps
	tag matching not supported
	num_comp_vectors:		64
		port:	1
			state:			PORT_ACTIVE (4)
			max_mtu:		4096 (5)
			active_mtu:		4096 (5)
			sm_lid:			0
			port_lid:		0
			port_lmc:		0x00
			link_layer:		Ethernet
			max_msg_sz:		0x80000000
			port_cap_flags:		0x04000000
			port_cap_flags2:	0x0000
			max_vl_num:		8 (4)
			bad_pkey_cntr:		0x0
			qkey_viol_cntr:		0x0
			sm_sl:			0
			pkey_tbl_len:		1
			gid_tbl_len:		128
			subnet_timeout:		0
			init_type_reply:	0
			active_width:		1X (1)
			active_speed:		25.0 Gbps (32)
			phys_state:		LINK_UP (5)
			GID[  0]:		fe80:0000:0000:0000:020e:1eff:fed4:227e, RoCE v1
			GID[  1]:		fe80::20e:1eff:fed4:227e, RoCE v2
			GID[  2]:		fe80:0000:0000:0000:020e:1eff:fed4:227e, RoCE v1
			GID[  3]:		fe80::20e:1eff:fed4:227e, RoCE v2
			GID[  4]:		fe80:0000:0000:0000:020e:1eff:fed4:227e, RoCE v1
			GID[  5]:		fe80::20e:1eff:fed4:227e, RoCE v2
			GID[  6]:		0000:0000:0000:0000:0000:ffff:ac1f:2d66, RoCE v1
			GID[  7]:		::ffff:172.31.45.102, RoCE v2
			GID[  8]:		0000:0000:0000:0000:0000:ffff:ac1f:2b66, RoCE v1
			GID[  9]:		::ffff:172.31.43.102, RoCE v2
			GID[ 10]:		0000:0000:0000:0000:0000:ffff:ac1f:2866, RoCE v1
			GID[ 11]:		::ffff:172.31.40.102, RoCE v2

hca_id:	qedr1
	transport:			iWARP (1)
	fw_ver:				8.59.1.0
	node_guid:			020e:1eff:fed4:227f
	sys_image_guid:			020e:1eff:fed4:227f
	vendor_id:			0x1077
	vendor_part_id:			5718
	hw_ver:				0x1
	phys_port_cnt:			1
	max_mr_size:			0x10000000000
	page_size_cap:			0xfffff000
	max_qp:				7936
	max_qp_wr:			32767
	device_cap_flags:		0x00209080
					CURR_QP_STATE_MOD
					RC_RNR_NAK_GEN
					MEM_MGT_EXTENSIONS
					Unknown flags: 0x8000
	max_sge:			4
	max_sge_rd:			4
	max_cq:				7936
	max_cqe:			8388480
	max_mr:				131070
	max_pd:				65536
	max_qp_rd_atom:			32
	max_ee_rd_atom:			0
	max_res_rd_atom:		0
	max_qp_init_rd_atom:		32
	max_ee_init_rd_atom:		0
	atomic_cap:			ATOMIC_GLOB (2)
	max_ee:				0
	max_rdd:			0
	max_mw:				0
	max_raw_ipv6_qp:		0
	max_raw_ethy_qp:		0
	max_mcast_grp:			0
	max_mcast_qp_attach:		0
	max_total_mcast_qp_attach:	0
	max_ah:				8192
	max_fmr:			0
	max_srq:			8192
	max_srq_wr:			32767
	max_srq_sge:			0
	max_pkeys:			0
	local_ca_ack_delay:		15
	general_odp_caps:
	rc_odp_caps:
					NO SUPPORT
	uc_odp_caps:
					NO SUPPORT
	ud_odp_caps:
					NO SUPPORT
	xrc_odp_caps:
					NO SUPPORT
	completion_timestamp_mask not supported
	core clock not supported
	device_cap_flags_ex:		0x209080
	tso_caps:
		max_tso:			0
	rss_caps:
		max_rwq_indirection_tables:			0
		max_rwq_indirection_table_size:			0
		rx_hash_function:				0x0
		rx_hash_fields_mask:				0x0
	max_wq_type_rq:			0
	packet_pacing_caps:
		qp_rate_limit_min:	0kbps
		qp_rate_limit_max:	0kbps
	tag matching not supported
	num_comp_vectors:		64
		port:	1
			state:			PORT_ACTIVE (4)
			max_mtu:		4096 (5)
			active_mtu:		1024 (3)
			sm_lid:			0
			port_lid:		0
			port_lmc:		0x00
			link_layer:		Ethernet
			max_msg_sz:		0x80000000
			port_cap_flags:		0x04000000
			port_cap_flags2:	0x0000
			max_vl_num:		8 (4)
			bad_pkey_cntr:		0x0
			qkey_viol_cntr:		0x0
			sm_sl:			0
			pkey_tbl_len:		0
			gid_tbl_len:		1
			subnet_timeout:		0
			init_type_reply:	0
			active_width:		1X (1)
			active_speed:		25.0 Gbps (32)
```
