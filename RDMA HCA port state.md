## Check the state by `ibstat` or `ibstatus`
```
$ ibstat
CA 'mlx5_0'
	CA type: MT4119
	Number of ports: 1
	Firmware version: 16.35.3006
	Hardware version: 0
	Node GUID: 0x248a070300a31964
	System image GUID: 0x248a070300a31964
	Port 1:
		State: Active
		Physical state: LinkUp
		Rate: 100
		Base lid: 6
		LMC: 0
		SM lid: 3
		Capability mask: 0xa659e84a
		Port GUID: 0x248a070300a31964
		Link layer: InfiniBand
CA 'mlx5_1'
	CA type: MT4119
	Number of ports: 1
	Firmware version: 16.35.3006
	Hardware version: 0
	Node GUID: 0x248a070300a31965
	System image GUID: 0x248a070300a31964
	Port 1:
		State: Active
		Physical state: LinkUp
		Rate: 100
		Base lid: 0
		LMC: 0
		SM lid: 0
		Capability mask: 0x00010000
		Port GUID: 0x268a07fffea31965
		Link layer: Ethernet
$ ibstatus
Infiniband device 'mlx5_0' port 1 status:
	default gid:	 fe80:0000:0000:0000:248a:0703:00a3:1964
	base lid:	 0x6
	sm lid:		 0x3
	state:		 4: ACTIVE
	phys state:	 5: LinkUp
	rate:		 100 Gb/sec (4X EDR)
	link_layer:	 InfiniBand

Infiniband device 'mlx5_1' port 1 status:
	default gid:	 fe80:0000:0000:0000:268a:07ff:fea3:1965
	base lid:	 0x0
	sm lid:		 0x0
	state:		 4: ACTIVE
	phys state:	 5: LinkUp
	rate:		 100 Gb/sec (4X EDR)
	link_layer:	 Ethernet

```
## Physical State
The physical state field indicates the state of the cable.  This is very similar to the link state on Ethernet.
- **Polling**
	1. No cable is connected to the adapter port: try to reconnect the cable and check the LED status.
	2. The cable is broken and needs to be replaced.
	3. The port on the other end of the cable is disabled
- **Disabled**
Enable the port using the ibportstate command
- **LinkUp**
There is link and connection between this node and the device at the other end of the cable.  This doesn’t mean it’s configured and ready to send data, just that the physical connection is up.

## State
Once the physical state for the port is set to LinkUp, the Subnet Manager needs to be started for the InfiniBand network to function

- Down
There is no physical connection between the HCA card in this node and the device at the other end of the cable.

-  Initializing
Physical connection has been made between the HCA in this node and the device at the other end of the cable, but it hasn’t been discovered by the subnet manager.  You need to make sure you have a managed switch, or more likely that the ‘opensm‘ process is running on a node in your cluster

- Active
The physical connection is up and working, and the port has been discovered by the subnet manager.  The port is in a normal operational state.

## References

- https://hasanmansur.com/2012/10/15/infiniband-troubleshooting/
- https://www.advancedclustering.com/act_kb/infiniband-port-states/
