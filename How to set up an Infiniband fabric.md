# How to set up an Infiniband fabric

_Take a Mellanox HCA as an example._

## Verify that the system has a Mellanox HCA
```
    [root@rdma03 ~]# lspci | grep -i Mell
07:00.0 Network controller: Mellanox Technologies MT27500 Family [ConnectX-3]

    [root@rdma04 ~]# lspci | grep -i Mell
07:00.0 Network controller: Mellanox Technologies MT27500 Family [ConnectX-3]
```
## Install modules
```
[root@rdma03 ~]# lsmod | grep mlx4
mlx4_core             315896  0 
devlink                60067  1 mlx4_core

[root@rdma03 ~]# modprobe -i mlx4_ib
[root@rdma03 ~]# modprobe -i ib_ipoib

[root@rdma03 ~]# lsmod | grep ib
ib_ipoib              115145  0 
ib_cm                  53122  1 ib_ipoib
mlx4_ib               179001  0 
ib_uverbs             102208  1 mlx4_ib
ib_core               255552  4 ib_cm,mlx4_ib,ib_uverbs,ib_ipoib
mlx4_core             315896  1 mlx4_ib
libata                243094  3 pata_acpi,ata_generic,ata_piix
devlink                60067  2 mlx4_ib,mlx4_core
libcrc32c              12644  2 xfs,bnx2x

Do the same things on rdma04
```

## Diagnostics
```
[root@rdma03 ~]# yum install -y infiniband-diags

[root@rdma03 ~]# ibstat
CA 'mlx4_0'
	CA type: MT4099
	Number of ports: 2
	Firmware version: 2.42.5000
	Hardware version: 1
	Node GUID: 0x0002c90300b3cff0
	System image GUID: 0x0002c90300b3cff3
	Port 1:
		State: Initializing
		Physical state: LinkUp
		Rate: 56
		Base lid: 0
		LMC: 0
		SM lid: 0
		Capability mask: 0x02514868
		Port GUID: 0x0002c90300b3cff1
		Link layer: InfiniBand
	Port 2:
		State: Initializing
		Physical state: LinkUp
		Rate: 56
		Base lid: 0
		LMC: 0
		SM lid: 0
		Capability mask: 0x02514868
		Port GUID: 0x0002c90300b3cff2
		Link layer: InfiniBand
```

 ## Subnet manager
```
[root@rdma03 ~]# systemctl enable opensm.service
Created symlink from /etc/systemd/system/network.target.wants/opensm.service to /usr/lib/systemd/system/opensm.service.
[root@rdma03 ~]# systemctl status opensm.service
● opensm.service - Starts the OpenSM InfiniBand fabric Subnet Manager
   Loaded: loaded (/usr/lib/systemd/system/opensm.service; enabled; vendor preset: disabled)
   Active: inactive (dead)
     Docs: man:opensm
[root@rdma03 ~]# systemctl start opensm.service
[root@rdma03 ~]# systemctl status opensm.service
● opensm.service - Starts the OpenSM InfiniBand fabric Subnet Manager
   Loaded: loaded (/usr/lib/systemd/system/opensm.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2019-11-26 01:16:53 EST; 1s ago
     Docs: man:opensm
  Process: 2400 ExecStart=/usr/libexec/opensm-launch (code=exited, status=0/SUCCESS)
 Main PID: 2401 (opensm-launch)
   CGroup: /system.slice/opensm.service
           ├─2401 /bin/bash /usr/libexec/opensm-launch
           └─2402 /usr/sbin/opensm

Nov 26 01:16:53 rdma03.rhts.eng.pek2.redhat.com opensm-launch[2400]: Log File: /var/log/opensm.log
Nov 26 01:16:53 rdma03.rhts.eng.pek2.redhat.com opensm-launch[2400]: -------------------------------------------------
Nov 26 01:16:53 rdma03.rhts.eng.pek2.redhat.com OpenSM[2402]: /var/log/opensm.log log file opened
Nov 26 01:16:53 rdma03.rhts.eng.pek2.redhat.com OpenSM[2402]: OpenSM 3.3.21
Nov 26 01:16:53 rdma03.rhts.eng.pek2.redhat.com opensm-launch[2400]: OpenSM 3.3.21
Nov 26 01:16:53 rdma03.rhts.eng.pek2.redhat.com opensm-launch[2400]: Using default GUID 0x2c90300b3cff1
Nov 26 01:16:53 rdma03.rhts.eng.pek2.redhat.com OpenSM[2402]: Entering DISCOVERING state
Nov 26 01:16:53 rdma03.rhts.eng.pek2.redhat.com opensm-launch[2400]: Entering DISCOVERING state
Nov 26 01:16:53 rdma03.rhts.eng.pek2.redhat.com OpenSM[2402]: Entering MASTER state
Nov 26 01:16:53 rdma03.rhts.eng.pek2.redhat.com opensm-launch[2400]: Entering MASTER state


[root@rdma03 ~]# ibnetdiscover
#
# Topology file: generated on Tue Nov 26 02:00:11 2019
#
# Initiated from node 0002c90300b3cff0 port 0002c90300b3cff1

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c90300b3c7c3
caguid=0x2c90300b3c7c0
Ca	2 "H-0002c90300b3c7c0"		# "rdma04 mlx4_0"
[1](2c90300b3c7c1) 	"H-0002c90300b3cff0"[1] (2c90300b3cff1) 		# lid 2 lmc 0 "MT25408 ConnectX Mellanox Technologies" lid 1 4xFDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c90300b3cff3
caguid=0x2c90300b3cff0
Ca	2 "H-0002c90300b3cff0"		# "MT25408 ConnectX Mellanox Technologies"
[1](2c90300b3cff1) 	"H-0002c90300b3c7c0"[1] (2c90300b3c7c1) 		# lid 1 lmc 0 "rdma04 mlx4_0" lid 2 4xFDR


[root@rdma03 ~]# iblinkinfo 
CA: rdma04 mlx4_0:
      0x0002c90300b3c7c1      2    1[  ] ==( 4X       14.0625 Gbps Active/  LinkUp)==>       1    1[  ] "MT25408 ConnectX Mellanox Technologies" ( )
CA: MT25408 ConnectX Mellanox Technologies:
      0x0002c90300b3cff1      1    1[  ] ==( 4X       14.0625 Gbps Active/  LinkUp)==>       2    1[  ] "rdma04 mlx4_0" ( )
```

## Verify the connection
```
[root@rdma04 ~]# ibping -S
[root@rdma03 ~]# ibping -c3 0002c90300b3c7c1
Pong from rdma04.rhts.eng.pek2.redhat.com.(none) (Lid 2): time 0.189 ms
Pong from rdma04.rhts.eng.pek2.redhat.com.(none) (Lid 2): time 0.253 ms
Pong from rdma04.rhts.eng.pek2.redhat.com.(none) (Lid 2): time 0.251 ms

--- rdma04.rhts.eng.pek2.redhat.com.(none) (Lid 2) ibping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 3000 ms
rtt min/avg/max = 0.189/0.231/0.253 ms
```

## IPoIB configuration
```
[root@rdma03 ~]# cat /etc/sysconfig/network-scripts/ifcfg-ib0 
CONNECTED_MODE=no
TYPE=InfiniBand
BOOTPROTO=static
IPADDR=172.31.2.3
DEVICE=ib0
ONBOOT=yes
[root@rdma03 ~]# ifup ib0
[root@rdma03 ~]# ip addr show ib0
8: ib0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 2044 qdisc pfifo_fast state UP group default qlen 256
    link/infiniband 80:00:02:08:fe:80:00:00:00:00:00:00:00:02:c9:03:00:b3:cf:f1 brd 00:ff:ff:ff:ff:12:40:1b:ff:ff:00:00:00:00:00:00:ff:ff:ff:ff
    inet 172.31.2.3/16 brd 172.31.255.255 scope global noprefixroute ib0
       valid_lft forever preferred_lft forever
    inet6 fe80::202:c903:b3:cff1/64 scope link 
       valid_lft forever preferred_lft forever

[root@rdma04 network-scripts]# cat ifcfg-ib0 
CONNECTED_MODE=no
TYPE=InfiniBand
BOOTPROTO=static
IPADDR=172.31.2.4
DEVICE=ib0
ONBOOT=yes

[root@rdma04 network-scripts]# ip addr show ib0
6: ib0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 2044 qdisc pfifo_fast state UP group default qlen 256
    link/infiniband 80:00:02:08:fe:80:00:00:00:00:00:00:00:02:c9:03:00:b3:c7:c1 brd 00:ff:ff:ff:ff:12:40:1b:ff:ff:00:00:00:00:00:00:ff:ff:ff:ff
    inet 172.31.2.4/16 brd 172.31.255.255 scope global noprefixroute ib0
       valid_lft forever preferred_lft forever
    inet6 fe80::202:c903:b3:c7c1/64 scope link 
       valid_lft forever preferred_lft forever

[root@rdma04 network-scripts]# ping -c3  172.31.2.3
PING 172.31.2.3 (172.31.2.3) 56(84) bytes of data.
64 bytes from 172.31.2.3: icmp_seq=1 ttl=64 time=0.191 ms
64 bytes from 172.31.2.3: icmp_seq=2 ttl=64 time=0.136 ms
64 bytes from 172.31.2.3: icmp_seq=3 ttl=64 time=0.137 ms

--- 172.31.2.3 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 1999ms
rtt min/avg/max/mdev = 0.136/0.154/0.191/0.029 ms
```
