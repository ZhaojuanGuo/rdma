The reason is that the difference between ibstat and ibstatus is that ibstat uses libumad to perform actual IB MAD packet send and receives to query ports.  
The cxgb3 card is an iWARP RNIC and as such does not support IB MAD packets. The ibstatus script uses the values found in /sys/class/infiniband to get port information 
and does not do anything with IB MAD packets, as a result it can successfully fill in the information on the cxgb3 card.


