## A script to do openmpi benchmark test setup
```Bash

$ cat openmpi-setup.sh 
#!/bin/bash

module purge
module load /usr/share/modulefiles/mpi/openmpi-x86_64
module list
systemctl stop firewalld.service

HOME="/root"
SERVER_IPV4=172.31.20.14
CLIENT_IPV4=172.31.20.15

# clean up any existing hostfiles
rm -f "${HOME}/hfile_one_core" "${HOME}/hfile_all_cores"
touch "${HOME}/hfile_one_core" "${HOME}/hfile_all_cores"

# create the single- and all- core hostfile
num_cores=$(grep -c ^processor /proc/cpuinfo)
num_cores=$(( num_cores - 1 ))
echo "$SERVER_IPV4" >> "${HOME}/hfile_one_core"
echo "$CLIENT_IPV4" >> "${HOME}/hfile_one_core"
for _ in $(seq 1 "$num_cores"); do
    echo "$SERVER_IPV4" >> "${HOME}/hfile_all_cores"
done
for _ in $(seq 1 "$num_cores"); do
    echo "$CLIENT_IPV4" >> "${HOME}/hfile_all_cores"
done

# add hfile_one_core and hfile_all_cores to .bashrc
sed -i '/export HFILES=/d' "${HOME}/.bashrc"
echo "export HFILES=\"${HOME}/hfile_one_core\"" >> "${HOME}/.bashrc"
source "${HOME}/.bashrc" 1>/dev/null 2>&1

```
## Some benchmark tests

```Bash
$ /usr/lib64/openmpi/bin/mpirun --allow-run-as-root --map-by node -mca btl_openib_warn_nonexistent_if 0 -mca btl_openib_if_include hfi1_0:1 -mca pml ucx -mca mtl psm2 -x PSM2_PKEY=0x8020 --mca pml_base_verbose 100 -mca btl '^openib' -mca osc ucx -x UCX_NET_DEVICES=hfi1_opa0 --mca osc_ucx_verbose 100 --mca pml_ucx_verbose 100 -hostfile /root/hfile_one_core -np 2 /usr/lib64/openmpi/bin/mpitests-IMB-EXT Window
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] mca: base: components_register: registering framework pml components
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] mca: base: components_register: found loaded component ucx
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] mca: base: components_register: component ucx register function successful
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] mca: base: components_open: opening pml components
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] mca: base: components_open: found loaded component ucx
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] pml_ucx.c:197 mca_pml_ucx_open: UCX version 1.14.1
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] mca: base: components_open: component ucx open function successful
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] mca: base: components_register: registering framework pml components
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] mca: base: components_register: found loaded component ucx
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] mca: base: components_register: component ucx register function successful
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] mca: base: components_open: opening pml components
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] mca: base: components_open: found loaded component ucx
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] pml_ucx.c:197 mca_pml_ucx_open: UCX version 1.14.1
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] mca: base: components_open: component ucx open function successful
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] select: initializing pml component ucx
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] pml_ucx.c:289 mca_pml_ucx_init
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] select: initializing pml component ucx
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] pml_ucx.c:289 mca_pml_ucx_init
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] pml_ucx.c:114 Pack remote worker address, size 38
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] pml_ucx.c:114 Pack local worker address, size 141
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] pml_ucx.c:351 created ucp context 0x55e8158b6ff0, worker 0x55e815a36850
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] select: init returned priority 51
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] selected ucx best priority 51
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] select: component ucx selected
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] pml_ucx.c:114 Pack remote worker address, size 38
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] pml_ucx.c:114 Pack local worker address, size 141
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] pml_ucx.c:351 created ucp context 0x5628e137bbb0, worker 0x5628e14cb920
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] select: init returned priority 51
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] selected ucx best priority 51
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] select: component ucx selected
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] pml_ucx.c:182 Got proc 0 address, size 141
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] pml_ucx.c:411 connecting to proc. 0
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] pml_ucx.c:182 Got proc 1 address, size 141
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] pml_ucx.c:411 connecting to proc. 1
#----------------------------------------------------------------
#    Intel(R) MPI Benchmarks 2021.3, MPI-2 part
#----------------------------------------------------------------
# Date                  : Tue Jul 25 03:37:21 2023
# Machine               : x86_64
# System                : Linux
# Release               : 5.14.0-339.el9.x86_64
# Version               : #1 SMP PREEMPT_DYNAMIC Thu Jul 13 07:33:32 EDT 2023
# MPI Version           : 3.1
# MPI Thread Environment: 


# Calling sequence was: 

# /usr/lib64/openmpi/bin/mpitests-IMB-EXT Window

# Minimum message length in bytes:   0
# Maximum message length in bytes:   4194304
#
# MPI_Datatype                   :   MPI_BYTE 
# MPI_Datatype for reductions    :   MPI_FLOAT
# MPI_Op                         :   MPI_SUM  
#
#

# List of Benchmarks to run:

# Window
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] pml_ucx.c:182 Got proc 1 address, size 38
[rdma-qe-15.rdma.lab.eng.rdu2.redhat.com:01940] pml_ucx.c:411 connecting to proc. 1
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] pml_ucx.c:182 Got proc 0 address, size 38
[rdma-qe-14.rdma.lab.eng.rdu2.redhat.com:02746] pml_ucx.c:411 connecting to proc. 0

#----------------------------------------------------------------
# Benchmarking Window 
# #processes = 2 
#----------------------------------------------------------------
       #bytes #repetitions  t_min[usec]  t_max[usec]  t_avg[usec]
            0          100       202.40       202.41       202.40
            4          100       202.46       202.48       202.47
            8          100       204.60       204.63       204.61
           16          100       202.03       202.05       202.04
           32          100       202.56       202.56       202.56
           64          100       203.15       203.16       203.15
          128          100       202.41       202.43       202.42
          256          100       203.76       203.79       203.77
          512          100       202.37       202.42       202.39
         1024          100       202.30       202.31       202.31
         2048          100       202.70       202.71       202.71
         4096          100       202.57       202.60       202.58
         8192          100       204.51       204.52       204.51
        16384          100       202.11       202.16       202.14
        32768          100       201.54       201.61       201.58
        65536          100       202.37       202.37       202.37
       131072          100       202.86       202.86       202.86
       262144          100       202.89       202.90       202.89
       524288           80       203.28       203.29       203.29
      1048576           40       203.68       203.71       203.69
      2097152           20       203.25       203.34       203.29
      4194304           10       206.81       206.90       206.85


# All processes entering MPI_Finalize

```
