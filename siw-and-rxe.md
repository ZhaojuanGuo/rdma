## siw

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/configuring_infiniband_and_rdma_networks/index

### Procedure
```
$ modprobe siw
$ rdma link add siw0 type siw netdev cxgb4_iw
$ rdma link show
$ ibv_devinfo siw0
```

### Known issues
bz2023416 - [RHEL-9.0] Mark Soft-iWARP (siw) as tech-preview

## rxe
### Procedure
```
$ modprobe rdma_rxe
$ rdma link add rxe_eno2 type rxe netdev eno2
$ rdma link | grep rxe_eno2
link rxe_eno2/1 state ACTIVE physical_state LINK_UP netdev eno2
$ ibv_devinfo -d rxe_eno2
hca_id:    rxe_eno2
    transport:   		 InfiniBand (0)
    fw_ver:   			 0.0.0
    node_guid:   		 2e44:fdff:fe93:b415
    sys_image_guid:   		 2e44:fdff:fe93:b415
    vendor_id:   		 0xffffff
    vendor_part_id:   		 0
    hw_ver:   			 0x0
    phys_port_cnt:   		 1
   	 port:    1
   		 state:   		 PORT_ACTIVE (4)
   		 max_mtu:   	 4096 (5)
   		 active_mtu:   	 1024 (3)
   		 sm_lid:   		 0
   		 port_lid:   	 0
   		 port_lmc:   	 0x00
   		 link_layer:   	 Ethernet
```
### Known issues
```
rxe is re-enabled as TECH PREVIEW in 9.3.
Bug 2022578 - [RHEL-9.1] Enable Soft-RoCE driver support in kernel config
$ modprobe rdma_rxe
$ dmesg
[ 1018.083229] TECH PREVIEW: Soft-RoCE (rdma_rxe) Driver may not be fully supported.
          	Please review provided documentation for limitations.
[ 1018.083418] rdma_rxe: loaded

```




