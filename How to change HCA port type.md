_Note: Different type of HCA might need different setup steps._

- Mellanox ConnectX-3 Adapter

```
Edit the file /etc/rdma/mlx4.conf:

Note: This file is read when the mlx4_core module is loaded and used to set the port types for any hardware found.

Format:

<pci_device_of_card> <port1_type> [port2_type]

port1 and port2: One of "auto", "ib", or "eth". port1 is required at all times, port2 is required for dual port cards.

For example:

 # echo 0000:06:00.0 ib ib > /etc/rdma/mlx4.conf

Perform reboot to reload the modules

 # reboot 
```

- Mellanox ConnectX-4 Adapter
```
In this example, the card is a Virtual Protocol Interconnect (VPI) card, and the LINK_TYPE_P1 and LINK_TYPE_P2 options control port mode. Available values are the following: 1 - IB, 2 - Ethernet, and 3 - VPI (autodetect).

~]$ sudo yum install mstflint

~]$ lspci | grep Mellanox

Query the selected card using the mstconfig command:
~]$ mstconfig -d 04:00.0 q

~]$ mstconfig -d 83:00.0 set LINK_TYPE_P1=2
Device #1:
----------

Device type:    ConnectX4       
PCI device:     83:00.0         

Configurations:                              Current         New
         LINK_TYPE_P1                        IB(1)           ETH(2)          

 Apply new Configuration? ? (y/n) [n] : 
Applying... Done!
-I- Please reboot machine to load new configurations.

```
- References
  * https://community.mellanox.com/s/article/howto-change-port-type-in-mellanox-connectx-3-adapter
  * https://access.redhat.com/articles/3082811
